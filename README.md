This uses Python 3.6 (tested with Anaconda 3.6.13).

Modules to install for Recognize.py:
    Librosa, soundfile, Youtube-dl,
	must have one of the two speech recognition engines it supports so far installed
	   (Google speechrecognition or Julius speech recognition), see above.
	In order to use either Bitchute downloader or Google speech recognition functionality,
	    it needs the vid2txt dependencies
	In order to use Julius speech recognition, it needs the ProcessJuliusCommand dependencies
	In order to download Youtube videos, it needs the Youtube-dl tool installed
	For further information, read below.
	
	Example commands / usage:
	
	"python Recognize.py -h"    (To see help / possible arguments for the command line)
    "python Recognize.py --videoUrl https://www.youtube.com/watch?v=oSSIBM1nEsE"  (will use julius)
	"python Recognize.py --videoUrl https://www.youtube.com/watch?v=oSSIBM1nEsE --engine google"
	
	The rest of the arguments really aren't necessary and probably shouldn't bother with changing
	the audioOutput argument right now, there are some quarks currently so it's no use changing.
	
	
--------------------------
Modules to install for vid2txt.py (currently supports both the Google speech recognizer call and
   downloading videos from Bitchute - will need to refactor these out to different modules later):

urllib, BeautifulSoup4, Google's SpeechRecognition, moviepy

To use this module by itself:  open a terminal or command line depending on operating system, 
run the script with the url of the video as the only argument:

"python vid2txt urlOfVideo"

It will parse the html, find the source of the video, download the video, 
convert it to audio file (wav), then use Google's speech recognition API to find all the spoken words
in the audio and output to a text file which you can then read.

--------------------------

Modules to install for ProcessJuliusCommand.py:
    Julius speech recognition (follow instructions at their github project page)
	Download their DNN speech model
	Note:  If any of the package dependencies will not install, use aptitude as such:
	   "sudo aptitude install packageName" - if there are packages it relies on that are higher versions
	      than required, it will ask you if it can do nothing.  
		  Just say no and follow the rest of the prompts.

    "python ProcessJuliusCommand.py pathToModelDirectory pathToJuliusBinDirectory"
	
	This module expects the audio file to already be in position "mozilla.wav".  To change the
	filename, make sure to put the new filename in test.dbl in the Julius models directory.

--------------------------