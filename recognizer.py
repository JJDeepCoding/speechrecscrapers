#This script will take in arguments and decide which of the subscripts to call.
#TODO:  Think about the differences in audio extraction from the different sites, try to smooth it out so users don't have
#       to bother with it / think about it

#vid2txt imports
import moviepy.editor as mp
import requests
import argparse
import urllib.request
from bs4 import BeautifulSoup as BS

#JuliusCommandScript imports
import os
from pathlib import Path
import sys
from sys import platform
from vid2txt import DownloadBitchuteVideo, ConvertVideo2Audio, GoogleSpeechRecognize, WriteResultTextfile
from processYoutubeDlCommand import processYoutubeDlCommand

import librosa  #used for resampling audio files
import soundfile as sf  #used by librosa to write out resampled audio files

from JuliusCommandScript import ProcessJuliusCommand, getAppropriateOStrailingSlash, OutputCompleteCheck, ParseResultFile

def handleSpeechRecognitionJulius(audioOutputFile, juliusModelDirectory, juliusBinDirectory):
    trailingSlash = getAppropriateOStrailingSlash()
    destinationAudioOutputFile = str(Path(juliusModelDirectory)) + trailingSlash + "mozilla.wav"
    exists = Path(destinationAudioOutputFile)
    if exists.is_file():
        removeCommand = "rm " + destinationAudioOutputFile
        print("Deleting mozilla.wav in preparation for moving the audio there before having Julius recognize it.")
        print(removeCommand)
        os.system(removeCommand)
    
    moveCommand = "mv " + audioOutputFile + " " + destinationAudioOutputFile
    print("Moving audio to be recognized to " + destinationAudioOutputFile + ": ")
    print(moveCommand)
    os.system(moveCommand)
    return ProcessJuliusCommand(juliusModelDirectory, juliusBinDirectory)

def handleAudio(videoOutputFile, audioOutputFile, audioExtractionRequired):
    if audioExtractionRequired:    
        ConvertVideo2Audio(videoOutputFile, audioOutputFile)  #get the audio from the video file and save it as a separate wav file
    #TODO:  Check sample rate and if not 16KHz, call resampleAudioFile()
    #https://stackoverflow.com/questions/43490887/check-audios-sample-rate-using-python
    #how to resample: https://stackoverflow.com/questions/30619740/downsampling-wav-audio-file
    #further documentation on the load function:  https://librosa.org/doc/latest/generated/librosa.load.html
    sr = librosa.get_samplerate(audioOutputFile)
    print("Audio sample rate (sr): " + str(sr))
    if sr != 16000:
        newSampleRate = 16000
        y_16k, s = librosa.load(audioOutputFile, sr=newSampleRate)  #it works for downsampling, not sure yet about upsampling
        print("New sample rate after load: " + str(s))
    #   y_16k = librosa.resample(y, sr, newSampleRate)  #fallback resample
        os.remove(audioOutputFile)   #Get rid of the audio file with the wrong sample rate for the speech recognition engines
        #librosa.output.write_wav(audioOutputFile, y_16k, newSampleRate)  #this method is deprecated / no longer present, replaced
                                                                            #by soundfile
        sf.write(audioOutputFile, y_16k, s)

def handleVideo(videoUrl, videoOutputFile, audioOutputFile):
    if "bitchute.com" in videoUrl.lower():
        DownloadBitchuteVideo(videoUrl, videoOutputFile)
    elif "youtube.com" in videoUrl.lower():
        videoFile = "" #going to grab audio only as it's an option with youtube-dl
        audioFormat = "wav"
        audioOutputFile = audioOutputFile
        processYoutubeDlCommand(videoUrl, videoFile, audioFormat, audioOutputFile)

#This will always be ran from the command line and is essentially a command parser itself
#Argument 1:  url of video
#Argument 2:  name of speech recognizer:  Google, Julius, etc (as I add functionality for more there will be more options)
def main():
    print("in main")
    parser = argparse.ArgumentParser()
    parser.add_argument("--videoUrl", help = "Add a url with a video, for instance Bitchute or Google")
    parser.add_argument("--engine", help = "Select a speech recognition engine, so far either Google or Julius", default="julius")
    parser.add_argument("--juliusModelDir", help = "Path to julius model directory, necessary for Julius recognition", 
        default='/home/jj/repos/speechrecognition/Julius/models/ENVR-v5.4.Dnn.Bin/')
    parser.add_argument("--juliusBinDir", help = "Path to julius bin directory, necessary for Julius recognition",
        default='/home/jj/repos/speechrecognition/Julius/julius/julius/')
    parser.add_argument("--videoOutputFile", help = "Name of video output file, optional", default="videoFile")
    parser.add_argument("--audioOutputFile", help = "Not used if speech recognizer is julius which uses only wav, optional", 
        default="audioOutput")
    #parser.add_argument("--audioOutputFile", help = "Name of audio output file, optional", default="converted.wav")
    parser.add_argument("--resultTextFile", help = "Name of text file with recognized speech, optional", default="output.txt")
    args = parser.parse_args()
    
    result = []
    
    audioExtractionRequired = handleVideo(args.videoUrl, args.videoOutputFile, args.audioOutputFile)
    #handleAudio(args.audioOutputFile, audioExtractionRequired)
    handleAudio(args.videoOutputFile, args.audioOutputFile + ".wav", False)
    
    if args.engine.lower() == "google":
        result = GoogleSpeechRecognize(args.outputAudioFileName)
    elif args.engine.lower() == "julius":
        result = handleSpeechRecognitionJulius(args.audioOutputFile + ".wav", args.juliusModelDir, args.juliusBinDir)
    
    
    #WriteResultTextfile(result, args.resultTextFile)

if __name__ == "__main__":
    main()