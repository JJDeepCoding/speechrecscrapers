import os
from pathlib import Path
import sys
from sys import platform
import argparse

def processYoutubeDlCommand(youtubeVideoUrl, videoOutputFileName = "videoFile", audioFormat = "wav", audioOutputFile = ""):
    if os.path.exists(videoOutputFileName):
        os.remove(videoOutputFileName)
    
    youtubeDlCommand = None
    if len(audioOutputFile) > 0:
        youtubeDlCommand = "youtube-dl -o " + audioOutputFile + ".%\(ext\)s -x --audio-format " + audioFormat + " " + youtubeVideoUrl
    else:
        youtubeDlCommand = "youtube-dl -o " + videoOutputFileName + " --audio-format " + audioFormat + " " + youtubeVideoUrl
    print(youtubeDlCommand)
    returnCode = os.system(youtubeDlCommand)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--videoUrl", help = "Url to video on Youtube, mandatory")
    parser.add_argument("--videoOutputFile", help = "Name of video output file, optional", default="videoFile")
    parser.add_argument("--format", help = "audio format, optional", default="wav")
    parser.add_argument("--audioOutputFile", help = "get only audio, optional", default="")
    args = parser.parse_args()
    
    print(args.videoUrl)
    print(args.videoOutputFile)
    print(args.format)
    print(args.audioOutputFile)
    processYoutubeDlCommand(args.videoUrl, args.videoOutputFile, args.format, args.audioOutputFile)

if __name__ == "__main__":
    main()