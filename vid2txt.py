#Reference help:  
#https://towarddatascience.com/extracting-speech-from-video-using-python-f0ec7e312d38
import moviepy.editor as mp
import requests
import argparse
import urllib.request
from bs4 import BeautifulSoup as BS

#Tested on Bitchute so far
def DownloadBitchuteVideo(bitchuteUrl, nameOfOutputVideo):
    #Get HTML page with video - pass url in at command line
    parser = argparse.ArgumentParser(description="Video url")
    parser.add_argument("url",  help="add argument with url for video")
    args = parser.parse_args()

    #Use urllib and BeautifulSoup to parse html and grab the video source element
    #https://stackoverflow.com/questions/24226781/changing-user-agent-in-python-3-for-urrlib-request-urlopen
    req = urllib.request.Request(
        args.url, 
        data=None, 
        headers={
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
        }
    )
    with urllib.request.urlopen(req) as response:
        html = response.read()
    soup = BS(html)
    source = soup.find("source")
    videoSrc = source.get('src')
    print(videoSrc)

    #1 Get the link and download it to a temp file here
    #reference https://likegeeks.com/downloading-files-using-python
    videoFile = requests.get(videoSrc)
    open(nameOfOutputVideo, 'wb').write(videoFile.content)

def ConvertVideo2Audio(videoFileName, audioFileName):
    #2 Convert the temp file to temp wav here
    #clip = mp.VideoFileClip(r"video_recording.mov")
    #clip = mp.VideoFileClip(r"videoFile")
    clip = mp.VideoFileClip(videoFileName)
    #clip.audio.write_audiofile(r"converted.wav")
    clip.audio.write_audiofile(audioFileName)

def GoogleSpeechRecognize(audioFile):
    import speech_recognition as sr
    #3 Run API on wav and generate a temp text file here with the transcript
    r = sr.Recognizer()
    audio = sr.AudioFile()

    with audio as source:
       audio_file = r.record(source)

    result = r.recognize_google(audio_file)
    return result

def WriteResultTextfile(result, resultTextFileName):
    #4 Export the recognized text as a text file
    with open(resultTextFileName, mode = 'w') as file:
       file.write("Recognized Speech:")
       file.write("\n")
       file.write(result)
       print("ready!")

def main():
    bitchuteUrl = ""
    outputVideoFile = "videoFile"
    outputAudioFile = "converted.wav"
    if len(sys.argv) > 1:
        if sys.argv[1] == "help":
            print("Example: python vid2txt.py https://www.bitchute.com/video/somethingsomething")
            print("Example2:  python vid2txt.py someBitchuteUrl nameOfOutputVideoFile")
            print("Example3:  python vid2txt.py someBitchuteUrl nameOfOutputVideoFile nameOfOutputAudioFile")
        else:
            bitchuteUrl = sys.argv[1]
    if len(sys.argv) > 2:
        outputVideoFile = sys.argv[2]
    if len(sys.argv) > 3:
        outputAudioFileName = sys.argv[3]
        
    else:
        print("Example: python vid2txt.py bitchute.com/video/somethingsomething")
        raise Exception("For further help, please use python vid2txt help")
    if bitchuteUrl != "":
        DownloadBitchuteVideo(bitchuteUrl)
        ConvertVideo2Audio(outputVideoFile, outputAudioFile)
        WriteResultTextfile(GoogleSpeechRecognize(outputAudioFile), 'recognized.txt')
    else:
        print("Please provide a bitchuteUrl as the first argument")

if __name__ == "__main__":
    main()