import os
from pathlib import Path
import sys
from sys import platform

#This will call the Julius Speech Recognition engine and return the phrase result array
def ProcessJuliusCommand(modelDirectory = '/home/jj/repos/speechrecognition/Julius/models/ENVR-v5.4.Dnn.Bin/', 
   juliusBinDirectory = '/home/jj/repos/speechrecognition/Julius/julius/julius/'):
    trailingSlash = getAppropriateOStrailingSlash()
    absolutePathToENVRdirectory = str(Path(modelDirectory).absolute()) + trailingSlash
    absolutePathToJuliusBinDirectory = str(Path(juliusBinDirectory).absolute()) + trailingSlash
    fileName = absolutePathToENVRdirectory + "output.txt"
    newName = absolutePathToENVRdirectory + "result.txt"
    if os.path.exists(newName):
        os.remove(newName)
        print("Removed file " + newName)
    print("Model directory: " + absolutePathToENVRdirectory)
    print("Julius bin directory: " + absolutePathToJuliusBinDirectory)
    command = 'julius -C julius.jconf -dnnconf dnn.jconf > output.txt'
    print("Final output will be in: " + newName)

    #this will start the Julius speech recognition engine - it looks at the test.dbl file in the model directory
    #in order to get the name of the audio file.  I will have preloaded that file with audio - note, it should already be a 16Khz audio file
    #in wav format (not sure what other formats Julius handles if any)
    #This means when extracting audio from video files, I'll have to check the sample rate and resample to 16KHz if necessary.
    currentDirectory = os.getcwd()
    os.chdir(absolutePathToENVRdirectory)
    print('Changed directory to: ' + absolutePathToENVRdirectory)
    print("Running command: " + absolutePathToJuliusBinDirectory + command)
    returnCode = os.system(absolutePathToJuliusBinDirectory + command)
    print("Return code from command: " + str(returnCode))
    outputHandled = False
    while outputHandled == False:
        if OutputCompleteCheck(fileName, newName):
            outputHandled = True
        else:
            sleep(5)
    result = ParseResultFile(newName)
    print("Phrases found: " + str(len(result)))
    print(result)
    os.chdir(currentDirectory) #change directory back to what it was when called
    return result

#necessary because:  https://bugs.python.org/issue21039
def getAppropriateOStrailingSlash():
    trailingSlash = ""
    if platform == "linux" or platform == "linux2":
        trailingSlash = "/"
    elif platform == "win32":
        trailingSlash = "\\\\"  #If this isn't quite correct, I am mostly guessing, haven't tested on Windows
    return trailingSlash

#Uses file existence and rename checks to see if output is complete
#If the file exists but cannot be renamed, it's still being worked on and this function will return false.
#If it can be renamed then it's not still open another process - return true.
#If the file does not exist, then raise an error.
def OutputCompleteCheck(fileName, newName):
    if os.path.exists(fileName):
        try:
            os.rename(fileName, newName)
            return True
        except:
            print("not ready yet")
            return False  #if we could not rename, then the file is open by another process
    else:
        raise NameError  #file with that name does not exist at this time

def ParseResultFile(fileName):
    result = []
    with open(fileName,'rt') as myfile:
        for line in myfile:
            if line.find('sentence1') != -1 and line.find('</s>') != -1:
                line1 = line.lstrip('sentence1: <s>').rstrip(' </s>\n')
                result.append(line1)
    return result

def main():
    modelDirectory = ""
    juliusBinDirectory = ""
    if len(sys.argv) > 2:
        modelDirectory = sys.argv[1]
        juliusBinDirectory = sys.argv[2]
        ProcessJuliusCommand(modelDirectory, juliusBinDirectory)
    else:
        print("Example usage: python pathToModelDirectory pathToJuliusBinDirectory")
        print("If you see directories starting with /home/jj in your output, that means you didn't supply paths and it likely won't match your system")
        print("Make sure to install Julius speech recognition system using instructions at:  https://github.com/julius-speech")
        print("You will need to download the DNN model directory, provide the path to where you downloaded it as the first argument")
        print("Provide the path to the binary folder for Julius as the second argument")
        ProcessJuliusCommand()

if __name__ == "__main__":
    main()